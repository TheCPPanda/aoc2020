import Text.Regex.TDFA
import Data.List
import Data.List.Split
import Data.Char

-- The regex for pid matches exactly 9 digits even when there are more.
-- so the pattern is a little bit ambigous.
validationPattern :: String
validationPattern = "(ecl:[a-z]{3})|(pid:[0-9]{9})|(eyr:[0-9]{4})|(hcl:#[0-9a-f]{6})|(byr:[0-9]{4})|(iyr:[0-9]{4})|(hgt:[0-9]+(cm|in))"

requiredFields = ["byr","ecl","eyr","hcl","hgt","iyr","pid"]

validateRange :: Int -> Int -> Int -> Bool
validateRange x lower upper = x <= upper && x >= lower

validateHeight :: String -> Bool
validateHeight x
  | isInfixOf "cm" x = validateRange num 150 193
  | isInfixOf "in" x = validateRange num 59 76
  | otherwise = False
  where
    num = read . takeWhile isDigit $ x

validateStructure :: String -> Bool
validateStructure info =  7 == length fields
  where fields = (getAllTextMatches (info =~ validationPattern) :: [String])


validateValue :: [String] -> Bool
validateValue ["byr", val] = validateRange (read val) 1920 2002
validateValue ["iyr", val] = validateRange (read val) 2010 2020
validateValue ["eyr", val] = validateRange (read val) 2020 2030
validateValue ["hgt", val] = validateHeight val
validateValue ["ecl", val] = elem val ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
validateValue xs = True

validateValues :: String -> Bool
validateValues xs = foldr (\acc valid -> acc && valid) validStructure . map validateValue . map (splitWhen (== ':')) $ getFields
  where
    validStructure = validateStructure xs
    getFields = getAllTextMatches (xs =~ validationPattern) :: [String]

main = do
  content <- readFile "input4.txt"
  putStrLn . show . length . filter validateValues . map concat . splitWhen (== "") . lines $ content
