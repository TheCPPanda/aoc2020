slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]

linearFunction :: Int -> Int -> Int
linearFunction m x = m * x

countTrees :: [(Int, String)] -> Int -> Int
countTrees xs m = foldr countTree 0 xs
  where
    countTree = (\(i, line) trees ->
      case (concat $ repeat line) !! ((linearFunction m i))  of
        '#' -> trees + 1
        _ -> trees + 0)

solve :: [String] -> (Int, Int) -> Int
solve xs (m, down) =
  let
    tiles = zip [0..] . map (\(_, s) -> s) . filter (\(i, _) -> i `mod` down == 0) $ zip [0..] xs
  in
    countTrees tiles m

main = do
  content <- readFile "input3.txt"
  let tiles = lines content
  
  putStrLn . show . foldr (*) 1 . map (solve tiles) $ slopes
