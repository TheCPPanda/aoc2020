generatePairs :: [Int] -> [(Int, Int, Int)]
generatePairs xs = [(x, y, z) | x <- xs, y <- xs, z <- xs]

sumIs2020 :: (Int, Int, Int) -> Bool
sumIs2020 (x, y, z) = x + y + z == 2020

solve :: [Int] -> Int
solve xs = head $ map (\(x, y, z) -> x * y * z) $ filter sumIs2020 $ generatePairs xs

main = do
  input <- readFile "input1.txt"
  let stuff = solve . map read . words
  putStrLn $ show $ stuff input
