import Data.List.Split

parseMinMax :: String -> (Int, Int)
parseMinMax xs =
  let [x, y] = splitOn "-" xs in
    (read x, read y)

countLetters :: String -> Char -> Int
countLetters str c = length $ filter (== c) str

foo :: Int -> Int -> Char -> String -> Bool
foo x y c str = (== 1) $ length [ x | (i, f) <- zip [1..] str, (i == x || i == y) && f == c]

applyRule :: Int -> Int -> Char -> String -> Bool
applyRule minC maxC c pw =
  let letterCount = countLetters pw c in
    letterCount >= minC && letterCount <= maxC

solve :: [String] -> Bool
solve [bounds, c, str] =
  let
    (m1, m2) = parseMinMax bounds
    chr = head c
  in
    foo m1 m2 chr str
    
main = do
  input <- readFile "input2.txt"
  putStrLn $ show $ head $ map words $lines input
  putStrLn $ show $ length $ filter solve $ map words $ lines input
